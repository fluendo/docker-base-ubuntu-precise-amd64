#!/bin/sh

# This script make a Docker image of the Ubuntu 12.04 Precise amd64 (64bit).
# Please run as root via sudo on your Ubuntu 14.04 LTS machine (Not docker container as AWS EC2).
# Reference: http://hatyuki.hatenablog.jp/entry/2014/11/20/135728
FOLDER=`pwd`

mkdir $FOLDER/tmp
cd $FOLDER/tmp
pwd
sudo apt-get update
sudo apt-get install -y git debootstrap binutils

# Fix version for a patch.
git clone --depth 1 -b v1.9.1 https://github.com/docker/docker.git
cd docker/contrib

# Make a Docker image from lucid http://archive.ubuntu.com/ubuntu/.
# Reference: https://hub.docker.com/_/ubuntu-debootstrap/
sudo ./mkimage.sh -d "../../" debootstrap --verbose --variant=minbase --components=main,universe --include=iproute,gpgv --arch=amd64 precise http://old-releases.ubuntu.com//ubuntu/

